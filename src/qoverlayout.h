#ifndef QOVERLAYOUT_H
#define QOVERLAYOUT_H

#include <QEvent>
#include <QGraphicsOpacityEffect>
#include <QParallelAnimationGroup>
#include <QPropertyAnimation>
#include <QSequentialAnimationGroup>
#include <QWidget>

/**
 * @brief A Qt layout overlay widget for anchored (Top, Right, Bottom and Left) widgets.
 *
 */
class QOverLayout : public QWidget
{
  Q_OBJECT

public:

  /**
   * @brief This enum describes how to render the widget, from one corner to another.
   */
  enum Anchor {TopLeft, TopRight, BottomRight, BottomLeft, None, Both};

  /**
   * @brief Constructs a widget which is a child of *parent*.
   *
   *        If *parent* is 0, the new widget becomes a window. If *parent* is another widget,
   *        this widget becomes a child window inside parent. The new widget is deleted when its parent is deleted.
   * @param parent
   */
  explicit QOverLayout(QWidget *parent = nullptr);

  /**
   * @brief Set widget pointer to your top widget and choose an anchor type.
   * @param widget* Pointer of the widget you want to anchor
   * @param anchor Type of anchor you want to use
   */
  void setTopWidget(QWidget *widget, const Anchor anchor = Anchor::Both);

  /**
   * @brief Set widget pointer to your right widget and choose an anchor type.
   * @param widget* Pointer of the widget you want to anchor
   * @param anchor Type of anchor you want to use
   */
  void setRightWidget(QWidget *widget, const Anchor anchor = Anchor::Both);

  /**
   * @brief Set widget pointer to your bottom widget and choose an anchor type.
   * @param widget* Pointer of the widget you want to anchor
   * @param anchor Type of anchor you want to use
   */
  void setBottomWidget(QWidget *widget, const Anchor anchor = Anchor::Both);

  /**
   * @brief Set widget pointer to your left widget and choose an anchor type.
   * @param widget* Pointer of the widget you want to anchor
   * @param anchor Type of anchor you want to use
   */
  void setLeftWidget(QWidget *widget, const Anchor anchor = Anchor::Both);

  /**
   * @brief Get the top animation pointer. Use it to set your own animation with setAnimation.
   *        msecs parameter is to configure the animation speed for the top animation.
   * @param msecs Animation speed
   * @return Return the top animation pointer
   */
  QParallelAnimationGroup *topAnimation(const int msecs = -1) const;

  /**
   * @brief Get the right animation pointer. Use it to set your own animation with setAnimation.
   *        msecs parameter is to configure the animation speed for the right animation.
   * @param msecs Animation speed
   * @return Return the right animation pointer
   */
  QParallelAnimationGroup *rightAnimation(const int msecs = -1) const;

  /**
   * @brief Get the bottom animation pointer. Use it to set your own animation with setAnimation.
   *        msecs parameter is to configure the animation speed for the bottom animation.
   * @param msecs Animation speed
   * @return Return the bottom animation pointer
   */
  QParallelAnimationGroup *bottomAnimation(const int msecs = -1) const;

  /**
   * @brief Get the left animation pointer. Use it to set your own animation with setAnimation.
   *        msecs parameter is to configure the animation speed for the left animation.
   * @param msecs Animation speed
   * @return Return the left animation pointer
   */
  QParallelAnimationGroup *leftAnimation(const int msecs = -1) const;

  /**
   * @brief Configure your own parallel animation when the layout is showned and hidden.
   * @param anim Pointer of your QParallelAnimationGroup.
   */
  void setAnimation(QParallelAnimationGroup *anim);

  /**
   * @brief Configure your own sequential animation when the layout is showned and hidden.
   * @param anim Pointer of your QSequentialAnimationGroup.
   */
  void setAnimation(QSequentialAnimationGroup *anim);

  /**
   * @brief Show anchored widgets with animation.
   */
  void slideAndShow();

  /**
   * @brief Hide anchored widgets with animation.
   */
  void slideAndHide();

signals:
  /**
   * @brief Size of layout changed
   */
  void sizeChanged();
  /**
   * @brief Layout starts show animation
   */
  void showing();
  /**
   * @brief Layout starts hide animation
   */
  void hiding();

protected:
  /**
   * @brief Override eventFilter to listen window resize(), mouseEnter() and mouseLeave() events.
   * @param object
   * @param event
   * @return
   */
  bool eventFilter(QObject *object, QEvent *event) override;

private:
  QWidget *topWidget = nullptr;
  QWidget *rightWidget = nullptr;
  QWidget *bottomWidget = nullptr;
  QWidget *leftWidget = nullptr;

  Anchor topWidgetAnchor = Anchor::Both;
  Anchor rightWidgetAnchor = Anchor::Both;
  Anchor bottomWidgetAnchor = Anchor::Both;
  Anchor leftWidgetAnchor = Anchor::Both;

  // Top Animation
  QPropertyAnimation *topAnimationSlide = nullptr;
  QPropertyAnimation *topAnimationOpacity = nullptr;
  QParallelAnimationGroup *topAnimationGroup = nullptr;

  // Right Animation
  QPropertyAnimation *rightAnimationSlide = nullptr;
  QPropertyAnimation *rightAnimationOpacity = nullptr;
  QParallelAnimationGroup *rightAnimationGroup = nullptr;

  // Bottom Animation
  QPropertyAnimation *bottomAnimationSlide = nullptr;
  QPropertyAnimation *bottomAnimationOpacity = nullptr;
  QParallelAnimationGroup *bottomAnimationGroup = nullptr;

  // Left Animation
  QPropertyAnimation *leftAnimationSlide = nullptr;
  QPropertyAnimation *leftAnimationOpacity = nullptr;
  QParallelAnimationGroup *leftAnimationGroup = nullptr;

  QParallelAnimationGroup *animation = new QParallelAnimationGroup(this);

  /**
   * @brief When *parent* widget (or windows) is resized, it resize layout geometry and anchored widget.
   */
  void resize();

  /**
   * @brief Called from resize(), resize top widget geometry.
   */
  void updateTopWidgetGeometry();

  /**
   * @brief Called from resize(), resize right widget geometry.
   */
  void updateRightWidgetGeometry();

  /**
   * @brief Called from resize(), resize bottom widget geometry.
   */
  void updateBottomWidgetGeometry();

  /**
   * @brief Called from resize(), resize left widget geometry.
   */
  void updateLeftWidgetGeometry();

};

#endif // QOVERLAYOUT_H
