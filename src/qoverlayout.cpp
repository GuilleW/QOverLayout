#include "qoverlayout.h"

QOverLayout::QOverLayout(QWidget *parent) : QWidget(parent)
{
  parent->installEventFilter(this);
}

void QOverLayout::setTopWidget(QWidget *widget, const Anchor anchor)
{

  topWidget = widget;

  switch ( anchor ) {
    case Anchor::TopLeft :
    case Anchor::TopRight:
    case Anchor::Both:
      topWidgetAnchor = anchor;
      break;
    default :
      topWidgetAnchor = Anchor::None;
    }
  topWidget->setParent(this);
  topWidget->show();

  // Opacity effect
  QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect(topWidget);
  opacityEffect->setOpacity(0.0);
  topWidget->setGraphicsEffect(opacityEffect);

  // Animation
  topAnimationSlide = new QPropertyAnimation(topWidget, "pos");
  topAnimationSlide->setDuration(250);
  topAnimationSlide->setEasingCurve(QEasingCurve::OutQuad);

  topAnimationOpacity = new QPropertyAnimation(opacityEffect, "opacity");
  topAnimationOpacity->setDuration(250);
  topAnimationOpacity->setEasingCurve(QEasingCurve::OutQuad);
  topAnimationOpacity->setStartValue(0.0);
  topAnimationOpacity->setEndValue(1.0);

  topAnimationGroup = new QParallelAnimationGroup(this);
  topAnimationGroup->addAnimation(topAnimationSlide);
  topAnimationGroup->addAnimation(topAnimationOpacity);
  animation->addAnimation(topAnimationGroup);

}

void QOverLayout::setRightWidget(QWidget *widget, const QOverLayout::Anchor anchor)
{

  rightWidget = widget;

  switch ( anchor ) {
    case Anchor::TopRight:
    case Anchor::BottomRight:
    case Anchor::Both:
      rightWidgetAnchor = anchor;
      break;
    default :
      rightWidgetAnchor = Anchor::None;
    }
  rightWidget->setParent(this);
  rightWidget->show();

  // Opacity effect
  QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect(rightWidget);
  opacityEffect->setOpacity(0.0);
  rightWidget->setGraphicsEffect(opacityEffect);

  // Animation
  rightAnimationSlide = new QPropertyAnimation(rightWidget, "pos");
  rightAnimationSlide->setDuration(250);
  rightAnimationSlide->setEasingCurve(QEasingCurve::OutQuad);

  rightAnimationOpacity = new QPropertyAnimation(opacityEffect, "opacity");
  rightAnimationOpacity->setDuration(250);
  rightAnimationOpacity->setEasingCurve(QEasingCurve::OutQuad);
  rightAnimationOpacity->setStartValue(0.0);
  rightAnimationOpacity->setEndValue(1.0);

  rightAnimationGroup = new QParallelAnimationGroup(this);
  rightAnimationGroup->addAnimation(rightAnimationSlide);
  rightAnimationGroup->addAnimation(rightAnimationOpacity);
  animation->addAnimation(rightAnimationGroup);

}

void QOverLayout::setBottomWidget(QWidget *widget, const QOverLayout::Anchor anchor)
{

  bottomWidget = widget;

  switch ( anchor ) {
    case Anchor::BottomLeft:
    case Anchor::BottomRight:
    case Anchor::Both:
      bottomWidgetAnchor = anchor;
      break;
    default :
      bottomWidgetAnchor = Anchor::None;
    }
  bottomWidget->setParent(this);
  bottomWidget->show();

  // Opacity effect
  QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect(bottomWidget);
  opacityEffect->setOpacity(0.0);
  bottomWidget->setGraphicsEffect(opacityEffect);

  // Animation
  bottomAnimationSlide = new QPropertyAnimation(bottomWidget, "pos");
  bottomAnimationSlide->setDuration(250);
  bottomAnimationSlide->setEasingCurve(QEasingCurve::OutQuad);

  bottomAnimationOpacity = new QPropertyAnimation(opacityEffect, "opacity");
  bottomAnimationOpacity->setDuration(250);
  bottomAnimationOpacity->setEasingCurve(QEasingCurve::OutQuad);
  bottomAnimationOpacity->setStartValue(0.0);
  bottomAnimationOpacity->setEndValue(1.0);

  bottomAnimationGroup = new QParallelAnimationGroup(this);
  bottomAnimationGroup->addAnimation(bottomAnimationSlide);
  bottomAnimationGroup->addAnimation(bottomAnimationOpacity);
  animation->addAnimation(bottomAnimationGroup);

}

void QOverLayout::setLeftWidget(QWidget *widget, const QOverLayout::Anchor anchor)
{

  leftWidget = widget;

  switch ( anchor ) {
    case Anchor::TopLeft:
    case Anchor::BottomLeft:
    case Anchor::Both:
      leftWidgetAnchor = anchor;
      break;
    default :
      leftWidgetAnchor = Anchor::None;
    }
  leftWidget->setParent(this);
  leftWidget->show();

  // Opacity effect
  QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect(leftWidget);
  opacityEffect->setOpacity(0.0);
  leftWidget->setGraphicsEffect(opacityEffect);

  // Animation
  leftAnimationSlide = new QPropertyAnimation(leftWidget, "pos");
  leftAnimationSlide->setDuration(250);
  leftAnimationSlide->setEasingCurve(QEasingCurve::OutQuad);

  leftAnimationOpacity = new QPropertyAnimation(opacityEffect, "opacity");
  leftAnimationOpacity->setDuration(250);
  leftAnimationOpacity->setEasingCurve(QEasingCurve::OutQuad);
  leftAnimationOpacity->setStartValue(0.0);
  leftAnimationOpacity->setEndValue(1.0);

  leftAnimationGroup = new QParallelAnimationGroup(this);
  leftAnimationGroup->addAnimation(leftAnimationSlide);
  leftAnimationGroup->addAnimation(leftAnimationOpacity);
  animation->addAnimation(leftAnimationGroup);

}

QParallelAnimationGroup *QOverLayout::topAnimation(const int msecs) const
{
  if( msecs >= 0) {
      topAnimationSlide->setDuration(msecs);
      topAnimationOpacity->setDuration(msecs);
    }
  return topAnimationGroup;
}

QParallelAnimationGroup *QOverLayout::rightAnimation(const int msecs) const
{
  if( msecs >= 0) {
      rightAnimationSlide->setDuration(msecs);
      rightAnimationOpacity->setDuration(msecs);
    }
  return rightAnimationGroup;
}

QParallelAnimationGroup *QOverLayout::bottomAnimation(const int msecs) const
{
  if( msecs >= 0) {
      bottomAnimationSlide->setDuration(msecs);
      bottomAnimationOpacity->setDuration(msecs);
    }
  return bottomAnimationGroup;
}

QParallelAnimationGroup *QOverLayout::leftAnimation(const int msecs) const
{
  if( msecs >= 0) {
      leftAnimationSlide->setDuration(msecs);
      leftAnimationOpacity->setDuration(msecs);
    }
  return leftAnimationGroup;
}

void QOverLayout::setAnimation(QParallelAnimationGroup *anim)
{
  animation = anim;
}

void QOverLayout::setAnimation(QSequentialAnimationGroup *anim)
{
  QParallelAnimationGroup *animP = new QParallelAnimationGroup(this);
  animP->addAnimation(anim);
  animation = animP;
}

void QOverLayout::slideAndShow()
{
  animation->setDirection(QAbstractAnimation::Forward);
  animation->start();
}

void QOverLayout::slideAndHide()
{
  animation->setDirection(QAbstractAnimation::Backward);
  animation->start();
}

bool QOverLayout::eventFilter(QObject *object, QEvent *event)
{
  if (event->type() == QEvent::Resize) {
      resize();
      emit sizeChanged();
    }
  else if(event->type() == QEvent::Enter){
      slideAndShow();
      emit showing();
    }
  else if(event->type() == QEvent::Leave){
      slideAndHide();
      emit hiding();
    }

  // standard event processing
  return QObject::eventFilter(object, event);

}

void QOverLayout::resize()
{

  // Resize layout to fit parent size
  setGeometry(0,0,parentWidget()->width(),parentWidget()->height());

  updateTopWidgetGeometry();
  updateRightWidgetGeometry();
  updateBottomWidgetGeometry();
  updateLeftWidgetGeometry();

}

void QOverLayout::updateTopWidgetGeometry()
{

  if(!topWidget) return;

  int w = width();
  int h = topWidget->maximumHeight();
  int x = 0;
  int y = 0;

  if( topWidgetAnchor == Anchor::TopLeft || topWidgetAnchor == Anchor::None ){
      if(rightWidget){
          w = w - rightWidget->maximumWidth();
        }

    }
  if( topWidgetAnchor == Anchor::TopRight || topWidgetAnchor == Anchor::None ){
      if(leftWidget){
          w = w - leftWidget->maximumWidth();
          x = x + leftWidget->maximumWidth();
        }
    }

  topWidget->resize(w,h);
  topAnimationSlide->setStartValue(QPointF(x,y-h));
  topAnimationSlide->setEndValue(QPointF(x,y));

}

void QOverLayout::updateRightWidgetGeometry()
{

  if(!rightWidget) return;

  int w = rightWidget->maximumWidth();
  int h = height();
  int x = width()-w;
  int y = 0;

  if( rightWidgetAnchor == Anchor::TopRight || rightWidgetAnchor == Anchor::None ){
      if(bottomWidget){
          h = h - bottomWidget->maximumHeight();
        }

    }
  if( rightWidgetAnchor == Anchor::BottomRight || rightWidgetAnchor == Anchor::None ){
      if(topWidget){
          h = h - topWidget->maximumHeight();
          y = y + topWidget->maximumHeight();
        }
    }

  rightWidget->resize(w,h);
  rightAnimationSlide->setStartValue(QPointF(x+w,y));
  rightAnimationSlide->setEndValue(QPointF(x,y));

}

void QOverLayout::updateBottomWidgetGeometry()
{

  if(!bottomWidget) return;

  int w = width();
  int h = bottomWidget->maximumHeight();
  int x = 0;
  int y = height()-h;

  if( bottomWidgetAnchor == Anchor::BottomLeft || bottomWidgetAnchor == Anchor::None ){
      if(rightWidget){
          w = w - rightWidget->maximumWidth();
        }

    }
  if( bottomWidgetAnchor == Anchor::BottomRight || bottomWidgetAnchor == Anchor::None ){
      if(leftWidget){
          w = w - leftWidget->maximumWidth();
          x = x + leftWidget->maximumWidth();
        }
    }

  bottomWidget->resize(w,h);
  bottomAnimationSlide->setStartValue(QPointF(x,y+h));
  bottomAnimationSlide->setEndValue(QPointF(x,y));

}

void QOverLayout::updateLeftWidgetGeometry()
{

  if(!leftWidget) return;

  int w = leftWidget->maximumWidth();
  int h = height();
  int x = 0;
  int y = 0;

  if( leftWidgetAnchor == Anchor::TopLeft || leftWidgetAnchor == Anchor::None ){
      if(bottomWidget){
          h = h - bottomWidget->maximumHeight();
        }

    }
  if( leftWidgetAnchor == Anchor::BottomLeft || leftWidgetAnchor == Anchor::None ){
      if(topWidget){
          h = h - topWidget->maximumHeight();
          y = y + topWidget->maximumHeight();
        }
    }

  leftWidget->resize(w,h);
  leftAnimationSlide->setStartValue(QPointF(x-w,y));
  leftAnimationSlide->setEndValue(QPointF(x,y));

}
