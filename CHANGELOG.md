# CHANGELOG

## v1.0.1

- Break the wall: Rename class to QOverLayout (All class I write in C++ Qt, will start by a Q)
- Update : example code is updated to use new class name

## v1.0.0

- NEW: Initial Commit
